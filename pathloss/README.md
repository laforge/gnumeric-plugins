path-loss plugin for gnumeric
=============================

This directory contains a python-language plugin for adding
functions to compute (Radio Frequency) path loss to the
[gnumeric](http://www.gnumeric.org/) spreadsheet software.

Using those functions, you can easily create spreadsheets performing
calculations around RF path loss, for example in order to estimate the
range of a radio transmitter (such as a cellular base station) according
to a variety of path loss models:

* Free-space path-loss
* [Okumura-Hata model](https://en.wikipedia.org/wiki/Hata_model_for_urban_areas) for open area, urban area and sub-urban area
* [Egli model](https://en.wikipedia.org/wiki/Egli_model) for outdoor line-of-sight

This plug-in was written in 2016 by Harald Welte <laforge@gnumonks.org>
as part of the [Osmocom](https://osmocom.org/) project for open source
mobile communications.


Installation
------------

You need a version of gnumeric that includes its python API.  This may
not be the case for all versions of gnumeric.  For example, on Debian
GNU/Linux, the python support is part of a separate
`gnumeric-plugins-extra` package that needs to be installed alongside
gnumeric.  So on Debian, you would need to install it using
`apt install gnumeric gnumeric-plugins-extra`

Once you have a python-capable gnumeric installed, you will need to
install this pathloss plugin.  To do so, copy the entire directory into
one of gnumeric's plugin directories.  The list of plugin directories
can be obtained from the gnumeric UI via the *Tools/Plug-Ins...* menu item
in the *Directories* tab.

The default directories on Linux systems are typically the system-wide
`/usr/lib/gnumeric/$version/plugins` and the per-user
`$HOME/.gnumeric/$version/plugins` directories.

So as a normal user, the usual strategy is to copy (or sym-link) the
path-loss directory into your personal `$HOME/.gnumeric/$version/plugins`.

Once you have copied it, you can start gnumeric and should have access
to the functions implemented here.


Usage
-----

The plug-in provides several gnumeric functions:

* PATHLOSS_FREESPACE(freq_mhz, dist_m)
* RF_RANGE_FREESPACE(freq_mhz, path_loss)
* PATHLOSS_EGLI(freq_mhz, dist_m, tx_ant_m, rx_ant_m)
* RF_RANGE_EGLI(freq_mhz, path_loss_db, bts_ant_m, ms_ant_m)
* PATHLOSS_HATA(environment, freq_mhz, dist_m, bts_ant_m, ms_ant_m)
* RF_RANGE_HATA(environment, freq_mhz, path_loss_db, bts_ant_m, ms_ant_m)

These functions can be used like any other spreadsheet function


### PATHLOSS_FREESPACE(freq_mhz, dist_m)

The function computes the free-space path-loss (in dB) given the following input parameters:

* *freq_mhz*, the radio frequency (in MHz)
* *dist_m*, the distance between transmitter and receiver (in meters)


### RF_RANGE_FREESPACE(freq_mhz, path_loss)

The function computes the free-space range (in meters) of a RF link given the following input parameters:

* *freq_mhz*, the radio frequency (in MHz)
* *path_loss*, the maximum permitted loss of your radio signal between transmitter and receiver (in dB)


### PATHLOSS_EGLI(freq_mhz, dist_m, tx_ant_m, rx_ant_m)

The function estimates the path-loss (in dB) according to the Egli model given the following input parameters:

* *freq_mhz*, the radio frequency (in MHz)
* *dist_m*, the distance between transmitter and receiver (in meters)
* *tx_ant_m*, the height of the transmitter antenna (in meters)
* *rx_ant_m*, the height of the receiver antenna (in meters)


### RF_RANGE_EGLI(freq_mhz, path_loss_db, bts_ant_m, ms_ant_m)

The function estimates the range (in meters) of a RF link according to the Egli model given the following input parameters:

* *freq_mhz*, the radio frequency (in MHz)
* *path_loss_db*, the maximum permitted loss of your radio signal between transmitter and receiver (in dB)
* *bts_ant_m*, the height of the transmitter antenna (in meters)
* *ms_ant_m*, the height of the receiver antenna (in meters)


### PATHLOSS_HATA(environment, freq_mhz, dist_m, bts_ant_m, ms_ant_m)

The function estimates the path-loss (in dB) according to the Okumura-Hata model given the following input parameters:

* *environment*, the environment (valid: open_area, rural, suburban, city_small, city_medium, city_large)
* *freq_mhz*, the radio frequency (in MHz)
* *dist_m*, the distance between transmitter and receiver (in meters)
* *bts_ant_m*, the height of the transmitter antenna (in meters)
* *ms_ant_m*, the height of the receiver antenna (in meters)


### RF_RANGE_HATA(environment, freq_mhz, path_loss_db, bts_ant_m, ms_ant_m)

The function estimates the range (in meters) of a RF link according to the Okumura-Hata model given the following input parameters:

* *environment*, the environment (valid: open_area, rural, suburban, city_small, city_medium, city_large)
* *freq_mhz*, the radio frequency (in MHz)
* *path_loss_db*, the maximum permitted loss of your radio signal between transmitter and receiver (in dB)
* *bts_ant_m*, the height of the transmitter antenna (in meters)
* *ms_ant_m*, the height of the receiver antenna (in meters)

Example
-------

An example spreadsheet is provided as `pathloss.gnumeric` in this directory.  It was created for
[sysmocom](https://sysmocom.de/) to estimate the range of a variety of cellular base station products.
