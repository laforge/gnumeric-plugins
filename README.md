gnumeric plugins
================

This repository hosts gnumeric plugins developed by Harald Welte <laforge@gnumonks.org>

So far, only one plugin has been written:  The [pathloss](pathloss/README.md) plugin for computing the path loss of radio links using several common models (Egli, Okumura-Hata).
